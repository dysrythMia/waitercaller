#!/usr/bin/env python

""" Default docstring: I can get a fish for a five cent worm """
__author__ = "DysrythMia"

from flask import Flask

app = Flask(__name__)

@app.route("/")
def home():
	return "Under Construction :)"

if __name__ == '__main__':
	app.run(port=5000, debug=True)